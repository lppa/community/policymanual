% POLICY MANUAL AND STANDING RULES
% LIBERTARIAN PARTY OF PENNSYLVANIA
% Adopted December 15, 2013

## Purpose and Scope
1. The purpose of the Party is to proclaim and implement the Statement of Principles of the National Libertarian Party through educational activities and participation in the political process.
2. The Party's ultimate goal is to achieve civil and economic liberty for Pennsylvanians while remaining true to the philosophical foundation of Libertarianism. All activities that are helpful in achieving that goal will be supported.

## MEMBERSHIP
Membership shall be established pursuant to the bylaws of the Party.
A. Currently, all membership inquireries are sent to the LPPA Chair, Both Vice-Chairs, and the Chair of the Membership Committee.

1. These inquiries will be handed off to the counties where the individuals requesting information reside.
2. In the event that there is no county or regional committee to hand these information requests to, it will be the responsibility of the vice chairs depending on which side of the state the request comes in from.
B. The membership database and all other policies regarding membership will be covered under the membership committee portion of section VIII

## COUNTY AND REGIONAL COMMITTEES
1. An Affiliate Committee Kit including documents to facilitate the establishment and function of County and Regional Committees shall be
developed and maintained.
2. The Board shall make local information in the database available to the committees upon request.
a)  The board, in conjunction with the Technology Oversight Committee and the Membership Committee shall develop a secure web-based database which any county or regional committee can access its own local data whenever necessary (as determined by the respective county or regional committee)
3. Each chapter, whether county or regional committee will be established in accordance with the bylaws.
4. The Party will work to help established committees and those wishing to form a committee with the goal of realizing committees in each of the 67 counties of Pennsylvania.

## OFFICERS (Executive Committee)
a) Chair: The Chair shall
1. become familiar with the Constitution, Bylaws, Platform, Convention Rules, and this Policy Manual
2. become familiar with Robert's Rules of Order Newly Revised and consult with a parliamentarian as needed;
3. ascertain the presence of a quorum at meetings;
4. open the meeting at the appointed time (if a quorum is present);
5. formulate the agenda of the meeting (with the help of the Board and in some cases, the membership);
6. announce in proper sequence the business that comes before the Board or Convention;
7. recognize those wishing to speak at board/business meetings;
8. state and put to vote all motions properly made, and to announce the result of the vote;
9. protect meeting from frivolous or dilatory motions; enforce rules of debate; expedite business;
10. decide questions of order;
11. appoint, with the advice of the board, a parliamentarian,
12. Assist the chairs of the various working and standing committees find volunteers to work / serve on the committees;
13. declare meeting adjourned at the time prescribed or at any time in the event of a sudden emergency;
14. call special meetings, telephone conferencing, or other Board of Directors votes when necessary;
15. follow-up on motions to ensure that they are put into effect;
16. Work with the Treasurer to ensure that actual expenses are in line with the money budgeted;
17. make reports to the regional representative of the Libertarian National Committee, LPPA Board of Directors, and others as required;
18. Serve the Party and the membership by helping to create organization and efficiency in function to all operations wherever and whenever possible. b) Vice-Chairs:

1. The Eastern Vice-Chair shall assist the development of the Party in the counties of the eastern part of Pennsylvania; the Western Vice-Chair shall assist the development of Party in the counties of the western part of Pennsylvania.
a. Both vice chairs shall work within the Grass Roots Committee and the Youth Outreach Committee so as to not duplicate effort, and to find volunteers that will help facilitate their efforts.
2. In the absence of the Party Chair, the Eastern Vice-Chair will preside at meetings in even years; the Western Vice-Chair will preside at meetings in odd years unless the Vice-Chairs come to their own arrangement.
3. The Vice-Chairs shall assist the Membership Committee, the Election Committee, and they will assist with the development of county and regional organizations.
4. The recruitment of candidates to seek the Libertarian Party nomination for partisan elected office, will be handled by the Grass Roots Committee and overseen by the Vice Chairs..
5. The Vice-Chairs will assist the Chair on various special projects as requested.
6. The Vice-Chairs will provide a report on their activities at each Board of Directors meeting.
c) Secretary: The Secretary shall
1. prepare meeting agendas and take the minutes of all the meetings;
2. keep on file all committee reports;
3. have access to all Party records;
4. Make the minutes and records available to members online; minutes posted on the website within five days of the adjournment of a meeting. Any amendments to the minutes will be handled as soon as possible but before the next board meeting
5. notify Board of Director members of upcoming votes or meetings (time, location);
6. sign (as necessary) all certified copies of acts of the Party;
7. maintain record books of the Articles of Incorporation, Bylaws, special rules of order, standing rules, minutes, or other important documents, and hold the corporate seal;
8. record the transaction of business and the result of votes at each Board of Directors meeting;
9. in the event of an absence of Chair or Vice Chair, call meeting to order and preside as chairman pro tem until the arrival of the Chair or a Vice Chair
10. file any necessary list of Party officers with the Department of State each year.
d) Treasurer: The Treasurer shall
1. have custody of the Party's funds in various bank or other accounts;
2. disburse funds as directed by the Board of Directors and Convention;
3. provide monthly accounting reports including beginning balance, income and expenditures for the time period, and ending balance and as necessary, Expense vs. Budget or other special reports.
4. file appropriate reports with government agencies as determined by the Board of Directors or applicable laws.

## BOARD OF DIRECTORS
1. The Board of Directors shall meet at least quarterly.
2. The Board of Directors shall be the governing body of the Party between conventions.
3. The Board of Directors shall with the help of the membership, adopt and implement policies and procedures to allow the continued function and growth of the Party.
4. Members of the Board of Directors must meet all requirements to hold the position and conduct themselves in a professional and civil manner during all Party business.
5. All reports, meeting agendas, and/or minutes shall be made available on-line, and so will the video archive of the live stream of each board meeting.
6. The Chair may call for a special meeting or vote on a particular issue as necessary pursuant to the provisions of the Bylaws.
7. Business may be conducted between meetings by telephone conference call, post, email, or in any other format agreed upon by the board.
8. Business will be conducted in accordance with the bylaws, and an archive that can be accessible, at any time, by the membership will be maintained in a place that is well marked to promote transparency and enable members to find such archives easily, at their convenience.
9. Communications Policy
a) The Libertarian Penn
1. The Libertarian Penn newsletter is a tremendous potential resource for the LPPA. The Editor of the Libertarian Penn shall not be a member of the board or in any position having a board vote in order to prevent any conflict of interest.
2. The publication schedule and content shall be determined by the Newsletter Committee, and the roles like "Editor" will be filled by members of the committee,
and each role will be determined by the committee.
3. The Newsletter Committee may choose to sell advertising, and any proceeds will be used to make the newsletter self-sufficient.
4. Content shall not be subject to veto by vote by the Executive Committee.
5. Distribution of the newsletter shall be determined by the Newsletter Committee based on available budget. When possible, it shall be mailed to all members in good standing, sent to the County / Regional Committees for dissemination, and it will also be made available on-line.
6. The Newsletter Editor and the Chairman of the LPPA each have veto power over final release of each released newsletter (sign-offs to happen before each release)
b) Website; www.lppa.org
1. The website of the LPPA, www.lppa.org
2. The site content and format shall be determined by the Website Committee with the advice of the general membership.
3. Included on the Website shall be:
a) updated LPPA and local Party contact info
b) A place for announcements from state, regional and county chapters.
c) Notice of the LPPA Convention
d) Current and back issues of the Libertarian Penn
e) Subscription information / instructions for any LPPA email lists
f) Membership form
g) Donation form
h) LPPA documents including but not limited to:
Platform, Constitution, Bylaws, Policy Manual, and Convention Rules.
i) A Webmaster may be appointed by the website Committee.
c.  e-mail lists shall be maintained as official means of communication between the LPPA and its membership. These list include, but are not limited to: LpBoard-Business@yahoogroups.com (Board Business) and LPBoard-Discussion@yahoogroups.com for discussion of items needing board action. LPPACCL@yahoogroups.com List for county committee discussion.
d.  Moderation
a.  There will be no moderation by board members or the chair
b.  Anyone who is abusive will be filtered out as spam by all individuals (including board members) who are offended by the abuser in question.
c.  Everyone here knows how to act as an adult. If everyone behaves as an adult, nobody will be forced to create e-mail filters. Previously there had been a complaint about disenfranchisement after a member swore at and insulted the secretary. The secretary did not disenfranchise this member, the member acted of his own free will, and the secretary acted in his. If an internet vote is void as a result of something like this happening in the future, such a vote will have to wait until the next board meeting.

## JUDICIAL COMMITTEE
1. The Judicial Committee members shall elect a Chair and notice of such election shall be made to the Board of Directors and to the website committee.
2. The Judicial Committee shall review any question of non-compliance with LPPA Constitution, Bylaws, put forward in writing to the Committee by any LPPA member in good standing to determine the need for committee action.
3. Chapter XX of Roberts Rules of Order Newly Revised, Disciplinary Procedures, is to be followed, as applicable.
4. The Committee shall act pursuant to the provisions of the bylaws

##    COMMITTEES
1. It is the responsibility of all Committee Chairs to see that any necessary Committee reports are presented to the Board of Directors.
a. County and regional affiliate committees are responsible for carrying out the activities of the Party at the local level. The officers of these affiliate committees are responsible for the operations and activities of the committees. Affiliate committees must operate in accordance with the provisions of the LPPA and LP bylaws.
2. Standing Committees
These Committees are established in the Bylaws and the composition may be altered by the Board of Directors.
a. The Membership Committee
1. This Committee shall develop the membership information packet. What is in this packet will be determined by the members of the committee using the various county committees as a resource for what would work best.
2. This Committee shall send out reminders regarding renewals and other dues related information.
3. This Committee shall maintain the database of current and past members, donors, volunteers, and contacts for no fewer than three years.
4. This Committee shall determine membership status of individuals upon authorized request. To include a membership committee volunteer to be on staff at the credentialing portion of the convention business meeting in order to facilitate that process.
b. The Media Relations Committee
1. The committee shall develop media releases on topics generated by the committee or requested by other officers of the Party.
2. The committee shall maintain the list of media contacts for the Party.
3. The committee shall send approved releases as directed.
4. All quotes in releases must be accurate, and if edited for any reason, edits must be approved by the person quoted.
5. Approval of releases can be done by the Chair of the Committee AND the LPPA Chair; by the Chair of the Committee AND both LPPA Vice-Chairs; or by a majority of the Board of Directors.
c. The Election (Ballot Access) Committee
1. The Committee shall develop an Election Handbook which will assist any Party member to run for any office in Pennsylvania.
2. The Committee shall recruit potential candidates for elected office and guide them through the nomination and ballot access process.
d. The Legal Action Committee
1. The Committee shall provide legal advice or inform the Board of Directors where they may seek appropriate legal advice regarding matters presented by the Board of Directors.
2. The Committee shall inform the Board of Directors of legal issues which the Board of Directors may not be aware.
3. The Committee shall keep a record of its legal advice to the party.
e. The Legislative Action Committee
1. The Committee shall keep track of especially important legislation for the membership in the Pennsylvania General Assembly online using whatever means appropriate to inform the largest number of members possible.
2. The Committee shall recommend action to the membership regarding specific pieces of legislation in the same fashion the committee would use in item 1.
3. The Committee shall develop, and work with other groups to develop, draft legislation to facilitate the motion of public policy in a libertarian direction consistent with the goals and philosophy of the Party.
4. The Committee shall seek sponsors of developed draft legislation and may contact elected officials and other groups as appropriate to garner support.
f. The Fundraising (Finance) Committee
1. The Committee shall engage in fundraising activities to support the needs of the Party to carry out its purpose.
2. The Committee shall advise the Board of Directors on the management of the Endowment Fund.
2. Working Committees
These Committees, with the exception of the Newsletter Committee are created and dissolved by the Board of Directors pursuant to the Bylaws. The LPPA chair may appoint or remove members to/from the working committees.
A. Marketing Committee
1) This committee shall identify and develop ways to promote the LPPA and its agenda to the general public.
2) This committee shall assist the Fund Raising Committee in identifying potential donors and contributors and develop strategies to build the base of Party supporters.
3) This committee will assist with Marketing Committee content for the website.
a. i.e. finding / creating marketing materials for county committees to use.
B. The Website Committee
1) This committee shall operate with the allocated budget.
2) This committee shall be responsible for the development of the Party's official website while looking for new ideas to keep the website fresh and interesting.
3) This committee shall make recommendations for hosting, maintenance, and upgrades as needed.
C. Newsletter Committee
1. The Committee shall prepare the Libertarian Penn newsletter for periodic publication.
2. The Committee shall determine the distribution, form, and the number of issues.
3. More on this committee (including the reasons for it to operate outside the typical working committee rules,) can be found within the communications policy located Section V item number 9 within this policy manual.
4. This committee shall operate using the Bylaws, Constitution, and the Philosophy of the party as a conscience for what it writes.
D. Youth Outreach Committee
1) This committee will be responsible for bringing the message of the LPPA to College and High School aged youth who are participating in liberty minded activities. i.e. "smokedown prohibition" "End The Fed" rallies, etc..
2) Members of this committee will be responsible for visitations to school / university Liberty clubs. i.e. Students for Liberty
3) This committee will find volunteer work for interested youth in order to bring them in, help them understand what we are about, and encourage growth within the party.
E. Platform Committee
1. This committee shall develop proposals regarding the Platform and/or Statement of Principles for consideration of delegates at the annual LPPA convention.
F. Convention Committee
1. The role of this Committee shall be generally filled by County Committees to prepare the annual LPPA Convention, and may be staffed as necessary.
2. The Convention Committee will be responsible for the details of the convention.
(The convention facilitator may or may not be a member of the Convention Committee depending on his/her interest)
a. Details will include, but will not be limited to:
i. Ensuring bylaws changes are available for the voting body.
ii. Figuring out who the members in good standing happen to be ahead of the convention.
iii. Ensuring that notification is sent and the convention is being handled in accordance with the Bylaws

## Membership Database Use Policy.
### Public data may be used as follows:
a.  Distribution of the Libertarian Penn Newsletter and any LPPA project mailing authorized by the Board of Directors.
b.  Any recognized county or regional committee may use member data within their respective electoral districts to pursue candidate activities or other political party activities.
c.  An approved candidate shall be an individual who has the nomination or formal
endorsement of any county/regional chapter, or if a candidate is in an area where there is no county/regional chapter, the endorsement/nomination would be coming from the LPPA.
d,  In addition, anyone pursuing a position in the state party, or is seeking the nomination/endorsement of county, regional, or the LPPA, may be approved by a county or regional committee, or, if it is a statewide request, or a request for an area where there is no active county or regional committee, the LPPA Board of Directors will be responsible for approving or denying such requests.
8.  Limitations on Use
a.  Authorized county or regional committee personnel may use LPP Member data in perpetuity.
b.  Endorsed candidates can use Public Data for the duration of a campaign.
c.  LPPA Member records will be provided consistent with restrictions specified in the Member Privacy Policy. Anyone who has access to the member database will, before receiving access, sign an NDA (Non-Disclosure Agreement)
9.  Request procedure
a.  Requests for member data shall be directed to the County or Regional Chapters if it is specific to a county or regional chapter, and for those requests made directly to the
LPPA, they may be made to the Membership Chair, or any other member of the Executive Committee.
b.  Requests shall include the Request/NDA form. If a completed form does not accompany an e-mail request, whomever receives the request will send the form to the requestor, and if this form is not returned, the request will not be considered.
c.  The keeper of member data is charged with distributing data in accordance with this policy and the Secretary of the LPP shall keep records of all requests for member data for a period of three years and shall periodically report to the Board of Directors on such requests.
d.  All requests for data shall be acted upon as soon as possible. If the requestor has not received a response within 1 calendar week, he/she should contact the chair directly
(unless any vice chairs wish to be contacted regarding this... Betsy? George?)

## CONVENTION
The annual LPPA convention will be held according to the arrangements of the Convention Committee pursuant to the provisions of the LPPA Constitution, Bylaws, and Convention Rules.

## NOMINATIONS OF CANDIDATES FOR OFFICE
1. The Election Committee may make recommendations to the Board of Directors as to the appropriateness of candidates to run as Libertarian Party nominees.
2. A candidate questionnaire has been prepared, and while it will be voluntary, everyone seeking the Libertarian nomination would do well by answering the questions.
3. At no time shall the Board of Directors nominate a candidate for district or local office prior to one year before an election for that office.
4. Procedure for district or local nominations by the Board of Directors in the absence of County or Regional Committees;
a. Prior to the Board of Directors nominating a district or local candidate in the absence of a county or regional organization, the LPPA Secretary shall, to the extent practical, notify LPPA members in the counties included in the district or local office of the request for nomination. This will be done prior to the nomination in an effort to receive input from the local Libertarians.
b. An announcement of the request for nomination shall be placed on the LPPA website. Nominations will be considered at Board Meetings, unless practical circumstances prevent this.
c. The nomination of a candidate other than at a regular meeting of the Board shall be conducted in accordance with the provisions of a special election pursuant to Article V, Section 2 of the bylaws.

## STATEMENT OF PRINCIPLES AND PLATFORM
All changes to the Statement of Principles and Party Platform shall be conducted in accordance with the Bylaws.

## AMENDMENTS
All amendments will be handled in accordance with Article XII of the Bylaws.

## PARLIAMENTARY AUTHORITY
The Parliamentarian shall act formally at the annual LPPA convention; and cannot vote (unless the vote is by ballot) or make motions or debate them-but may provide points of information, as well as provide parliamentary opinions. The Parliamentarian shall act informally at Board of Director meetings and so may vote, make motions, and debate if a member of the Board of Directors. The Parliamentarian shall be familiar with the latest edition Robert's Rules of Order Newly Revised.

## Data Maintenance Policy
Nobody shall have any authority to delete party records. Once a record is made having anything to do with party business, said record shall become property of the party and
thus subject to this policy. However we enable multiple copies to be maintained we will make it happen.
