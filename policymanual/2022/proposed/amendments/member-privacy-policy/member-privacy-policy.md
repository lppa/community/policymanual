# Member Privacy Policy

This website is owned by the Libertarian Party of Pennsylvania (LPPA)

We respect your privacy, and we want you to know what information is (and is not) collected on this website, and how it is (and is not) used.

We can be reached by email at privacy@lppa.org.

This Website collects some Personal Data from its Users.

The Libertarian Party of Pennsylvania (LPPA) is committed to protecting the privacy of visitors to our website, as well as our members and volunteers. LPPA has established this Privacy Policy to explain what information we collect through our websites and how it is used, as well as protections for our members’ and donors’ personal information and protect Personally Identifiable Information (PII) from abuse.

LPPA's Use of Information

In general, LPPA uses the information provided by you to further its platform, including to advocate for liberty, defend freedom and innovation, and to protect your rights in the political world.

We may share information with authorized organizational divisions as well as legitimate data processors. 

As a member, your Personally Identifiable Information (PII) shall be protected from public release, unless you voluntarily give your explicit consent by written or electronic means, or otherwise proscribed by law.
