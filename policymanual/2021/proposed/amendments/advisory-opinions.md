## Section VI Article 2

[JUDICIAL COMMITTEE]

\\ OLD VERSION

2. a. The Judicial Committee shall review any question of non-compliance with LPPA Constitution, Bylaws, put forward in writing to the Committee by any LPPA member in good standing to determine the need for committee action.

\\ NEW VERSION

2. a. Any member in good standing may request to appeal a decision of the Board on grounds of LPPA Constitution, Bylaws non-compliance. The Committee shall review a written appeal pursuant to the provisions of the rules.
 
+    b. Requests must include a list the material facts and circumstances surrounding the request.

+    c. Any member in good standing subject LPPA Constitution, Bylaws may request an advisory opinion about his or her own obligations. An Advisory may also be requested by the authorized representative of such person. The Committee shall review a question pursuant to the provisions of the rules. 

+    d. Requests must include a list the material facts and circumstances surrounding the request.

+    e. No person who acts in good faith on an opinion that was issued to him by the Committee may be subject to discipline or other penalties for so acting, provided that he truthfully disclosed all material facts in the opinion requested.

+    f. An advice issued by the Committee is a complete defense in any LPPA disciplinary proceeding and evidence of good faith conduct if the advice was requested at least 21 working days prior to taking the action described in the request, the material facts were as stated in the request, and the subject of the request committed the acts complained of either in reliance on the advice or because of the Committee failure to provide advice within the 21 working day timeframe or extended deadline.

