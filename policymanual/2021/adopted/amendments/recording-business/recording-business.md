Board business will be conducted in accordance with the bylaws. Minutes shall be maintained indefinitely. 

Business conducted during meetings shall be recorded, with the exception of executive session or a suspension of the rules.

A disclaimer should be provided to join each business meeting and advertised during the meeting, informing the meeting attendees of the recording.

Business meetings shall be recorded using LPPA provided equipment and/or software, and maintained using LPPA persistent storage.

Board members may request access to the recordings within 90 days of the meeting. Requests should include a signed NDA from the requestor and be logged. Recordings shall be deleted after 60 days of approval of the minutes, unless there is a litigation hold requiring preservation of records.  
