## VII.  COMMITTEES 
3. Standing Committees 

   The Committees listed below are established in the Bylaws and their composition is determined by the Board of Directors. The Board has the authority to approve or remove committee members at any time by majority vote.

   Standing Committees shall be composed of no more than seven voting members. Committees shall be empowered to enlist people to help them in their work, with the approval of the Committee Chair, but these additional people shall not be voting members. Where particular tasks require, a working committee may be created by the chair to perform those tasks

   The Board of Directors may review, from time to time, the performance of these committees, and the duties to which they are charged.

   (List of standing committees and their responsibilities).
 
4. Working Committees

   These Committees, with the exception of the Newsletter Committee, are created and dissolved by the Chair pursuant to the Bylaws. The LPPA chair may appoint or remove members to/from the working committees.

   As with standing committees above, there shall be no more than seven voting members in a working committee. These members are appointed by the Board Chair. Additional people helping in the committee are not considered voting members. The seven (7) voting members for each standing committees shall be elected semi-annually by all of the members of that committee at the time the election is held.
