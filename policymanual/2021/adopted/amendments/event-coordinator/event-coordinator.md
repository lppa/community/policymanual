VIII. COORDINATORS
2) It is the responsibility of a Event Coordinator to work with state and local affiliate secretaries to notify subscribers of upcoming events. This may include updating the event calendar, sending reminder messages, and teaching new secretaries how to manage events themselves.
