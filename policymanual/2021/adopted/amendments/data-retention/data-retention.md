## XV. Data Maintenance Policy
    
Nobody shall have any authority to delete approved party records. Once a record is approved by the board, having anything to do with party business, said record shall become property of the party and thus subject to this policy. However, we may enable multiple copies to be maintained.
