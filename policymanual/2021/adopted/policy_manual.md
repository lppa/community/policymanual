% POLICY MANUAL AND STANDING RULES of the
% LIBERTARIAN PARTY OF PENNSYLVANIA
% Adopted March 20, 2021

## I. Purpose and Scope

1.  The purpose of the Party is to proclaim and implement the Statement of Principles of the National Libertarian Party through educational activities and participation in the political process.

2.  The Party's ultimate goal is to achieve civil and economic liberty for Pennsylvanians while remaining true to the philosophical foundation of Libertarianism. All activities that are helpful in achieving that goal will be supported.

## II. Membership

Membership shall be established pursuant to the bylaws of the Party.

A.  Membership inquiries shall be sent to a representative selected by the Membership Committee. The Chair and Vice Chairs shall be copied as well as the Executive Director if one is employed.

1.  It shall be at the discretion of the Membership Committee and the Vice Chairs whom shall be responsible for handing these inquiries to the counties.

2.  This committee shall maintain an Affiliate Committee Kit including documents to facilitate the establishment and function of County and Regional Committee.

B.  The CR database and all other policies regarding membership will be covered under the membership committee portion of section VIII.

1.  The contacts selected by the group established in II. Section A shall make local information in the database available to the committees upon request.

## III. COUNTY AND REGIONAL COMMITTEES

1.  Each chapter, whether county or regional committee will be established in accordance with the bylaws.

2.  Individuals named in II. Section 1 shall work to help increase membership in established committees and locate members wishing to form new affiliates in counties were no committee exists.

## IV. OFFICERS (Executive Committee)

a) Chair: The Chair shall

1. become familiar with the Constitution, Bylaws, Platform, Convention

Rules, this Policy Manual, and Roberts Rules of Order Newly Revised.

2.  Handle all aspects required to run a business meeting.

3. Assist the Vice-Chairs as requested, and assist the chairs of the various working and standing committees.

a.  To include but not limited to:

    i.  Recruiting volunteers to work / serve on committees

    ii. Obtaining resources to help committees do the work they need to do.

3.  Call special meetings as needed.

4.  Follow up to ensure that the will of the board is being executed.

5.  Work with the Treasurer and Finance Committee Chair to ensure that Party expenses are in line with the money budgeted.

6.  Write reports outlining the activities of the party as needed.

b) Vice-Chairs:

1. The Vice-Chairs shall assist the development of the Party

2. In the absence of the Party Chair,

3. Vice-Chairs shall assist the Membership and Election Committees, and they will assist with the development of county and regional organizations.

4. The recruitment of candidates to seek the Libertarian Party nomination for partisan elected office will be handled by the Elections Committee and overseen by the Vice Chairs.

5. The Vice-Chairs will assist the Chair on various special projects as requested.

6. The Vice-Chairs will provide a report on their activities at each Board of

Directors meeting.

c) Secretary: The Secretary shall

1. take the minutes of all the meetings;

2. keep on file all committee reports;

3. have access to all Party records;

4. Make the minutes and records available to members online; minutes shall be posted on the website within five days of the adjournment of a meeting. Any amendments to the minutes will be handled as soon as possible, but before the next board meeting.

6. sign (as necessary) all certified copies of acts of the Party; and

7. hold the corporate seal;

8. record the transaction of business and the result of votes at each Board of Directors meeting;

9. file any necessary list of Party officers with the Department of State each year.

d) Treasurer: The Treasurer shall

1. have custody of the Party's funds in various bank or other accounts;

2. disburse funds as directed by the Board of Directors and Convention;

3. provide monthly accounting reports including beginning balance, income and expenditures for the time period, and ending balance and as necessary, Expense vs. Budget or other special reports.

4. file appropriate reports with government agencies as determined by the Board of Directors or applicable laws.

e) County / Region Board Reps shall:

1.  Communicate to the IS Chair that they were appointed;

2.  Communicate to their respective committees what is happening with Board decisions and how those decisions will affect the party at the county level;

1.  All communication between the board and the county committees shall be the responsibility of the board rep even in cases where other board members are attending county meetings.

## V. BOARD OF DIRECTORS (BOD)

1. The BOD shall meet at least quarterly.

2. The BOD shall be the governing body of the Party between conventions.

4. Members of the Board of Directors must meet all requirements to hold the position and conduct themselves in a professional and civil manner during all Party business.

5. All reports, meeting agendas, and/or minutes shall be made available on-line, and so will the video archive of the live stream of each board meeting.

6. Business may be conducted between meetings, (as called by the Chair) by electronic meeting, or in any other format agreed upon by the board, pursuant to the provisions of the Bylaws. (i.e. 14 days notice)

7. Board business will be conducted in accordance with the bylaws. Minutes shall be maintained indefinitely. 

   Business conducted during meetings shall be recorded, with the exception of executive session or a suspension of the rules.

   A disclaimer should be provided to join each business meeting and advertised during the meeting, informing the meeting attendees of the recording.

   Business meetings shall be recorded using LPPA provided equipment and/or software, and maintained using LPPA persistent storage.

   Board members may request access to the recordings within 90 days of the meeting. Requests should include a signed NDA from the requestor and be logged. Recordings shall be deleted after 60 days of approval of the minutes, unless there is a litigation hold requiring preservation of records.


9. Communications Policy

*a) The Libertarian Penn*

2. The publication schedule and content shall be determined by the Newsletter Committee.

3. The Newsletter Committee may choose to sell advertising; any proceeds will be used to make the newsletter self-sufficient.

4. Content shall not be subject to veto by vote by the Executive Committee.

5. All aspects of the Distribution process for the newsletter shall be handled by the Newsletter Committee. When possible, it shall be mailed to all members in good standing, sent to the County / Regional Committees for dissemination, and a copy shall be maintained on-line.

6. The Newsletter Editor and the Chairman (or Executive Director) of the LPPA each have veto power over final release of each released newsletter (sign-offs to happen before each release).

b) Website

1)  The website of the LPPA is ["lppa.org](http://www.lppa.org/)"

2)  Content shall be informed by county and committee activity

3. Included on the Website shall be:

a) Updated LPPA and local Party contact info

b) A place for announcements from state, regional and county chapters

c) Notice of the LPPA Convention

d) Current and back issues of the Libertarian Penn

e) Subscription information / instructions for any

LPPA email lists

f) Membership form

g) Donation form

h) LPPA documents including but not limited to: Platform, Constitution, Bylaws, Policy Manual, and Convention Rules.

i) A Webmaster may be appointed by the IS Committee.

c. e-mail lists shall be maintained as official means of communication between the LPPA and its membership. These lists include: <lppa-md1@lists.lppa.org>[,]{.ul} the official discussion list for members to communicate items requiring board action, and the board discussion list: <lppa-bod@lists.lppa.org> This list shall be used as official communication among the members of the Board of Directors. While no member may comment directly to that list, members shall be able to view what is being discussed by the board on the member discussion list in the form of a daily digest.

-   Individual board members' social media accounts shall not be considered an official communication channel between the board and the membership.

-   Slack may be used as an unofficial mode of communication for Committee or Board Members.

1.  Should there need to be a vote taken by e-mail, (an example being something discussed at a board meeting, but not having all of the information to vote during the meeting) the board shall determine how much notice is needed, and then, once the voting begins, there shall be 48 hours for all members of the BOD to vote. Any member of the BOD may change his / her vote at any time during the voting process up until the vote is closed.

## VI. JUDICIAL COMMITTEE

1. The Judicial Committee members shall elect a Chair; notice of such election shall be made to the Board of Directors and to the IS Committee.

2. The Judicial Committee shall review any question of non-compliance with LPPA Constitution, Bylaws, put forward in writing to the Committee by any LPPA member in good standing to determine the need for committee action.

3. In the event an action is not covered by Article 2 Section 3 of the Bylaws, Chapter XX of Roberts Rules of Order Newly Revised, Disciplinary Procedures, is to be followed, as applicable.

4. The Committee shall act pursuant to the provisions of the bylaws.

## VII. COMMITTEES

1. It is the responsibility of all Committee Chairs to see that any necessary Committee reports are presented to the Board of Directors.

a. County and regional affiliate committees are responsible for carrying out the activities of the Party at the local level. The officers of these affiliate committees are responsible for the operations and activities of the committees. Affiliate committees must operate in accordance with the provisions of the LPPA bylaws.

2. It is the function of Committee Chairs to promptly employ volunteers, as needed.

3. Standing Committees

   The Committees listed below are established in the Bylaws and their composition is determined by the Board of Directors. The Board has the authority to approve or remove committee members at any time by majority vote.

   Standing Committees shall be composed of no more than seven voting members. Committees shall be empowered to enlist people to help them in their work, with the approval of the Committee Chair, but these additional people shall not be voting members. Where particular tasks require, a working committee may be created by the chair to perform those tasks

   The Board of Directors may review, from time to time, the performance of these committees, and the duties to which they are charged.

a. Membership Committee

   (In addition to what was listed in section II)

1. The Membership Committee shall handle all communication between the party and membership regarding new memberships and renewals. This shall include reaching out to members whose memberships have been lapsed for up to 4 years.

3. This Committee shall use the CRM to help maintain accurate membership records.

4. Members of this Committee shall inform the credentialing committee for annual Libertarian Party of Pennsylvania Conventions in conjunction with the Treasurer.

b. The Information Services (IS) Committee

1) The Committee shall review and operate within its allocated budget.

2) The Committee shall be responsible for the development of all the Party's official Information Systems and Information Technology (IT) needs.

4) The Committee should, where practical, allow for the creation of new user accounts for State Leaders, County/ Regional Leaders, and Volunteers.

4) The Committee should assign access level controls to help manage security and privacy, as needed.

5) The Committee should implement hosting, maintenance, and upgrades as needed.

c. The Media Relations Committee

1. The committee shall develop media releases on topics generated by the committee or requested by other officers of the Party.

2. The committee shall use the CRM to maintain the list of media contacts for the Party.

3. The committee shall send approved releases as directed.

4. All quotes in releases must be accurate, and if edited for any reason, edits must be approved by the person quoted.

5. Press Releases may be approved and sent out by the Chair of Media Relations if the LPPA chair, Vice Chairs, or a majority of the board of directors authorize the Chair of Media Relations to use his/her discretion ahead of time.

d. The Election (Ballot Access) Committee

1. The Committee shall develop an Election Handbook which will assist any

Party member to run for any office in Pennsylvania.

2. The Committee shall recruit potential candidates for elected office and guide them through the nomination and ballot access process.

3. The Committee should use the CRM to help maintain accurate political campaign records.

e. The Legal Action Committee

1. The Committee shall provide legal advice or inform the Board of Directors where they may seek appropriate legal advice regarding matters presented by the Board of Directors.

2. The Committee shall inform the Board of Directors of legal issues of which the

Board of Directors may not be aware.

3. The Committee shall keep a record of its legal advice to the party.

f. The Legislative Action Committee

1. The Committee shall keep track of especially important legislation for the membership in the Pennsylvania General Assembly online using whatever means appropriate to inform the largest number of members possible.

2. The Committee shall recommend action to the membership regarding specific pieces of legislation in the same fashion as the committee would use in item 1.

3. The Committee shall develop, and work with other groups to develop, draft legislation to facilitate the motion of public policy in a libertarian direction consistent with the goals and philosophy of the Party.

4. The Committee shall seek sponsors of developed draft legislation and may contact elected officials and other groups as appropriate to garner support.

g. The Fundraising (Finance) Committee

1. The Committee shall engage in fundraising activities to support the needs of the Party to carry out its purpose.

2. The Committee shall advise the Board of Directors on the management of the Endowment Fund.

3. The Committee should use the CRM to help maintain accurate fundraising campaign records.

4. Working Committees

   These Committees, with the exception of the Newsletter Committee, are created and dissolved by the Chair pursuant to the Bylaws. The LPPA chair may appoint or remove members to/from the working committees.

   As with standing committees above, there shall be no more than seven voting members in a working committee. These members are appointed by the Board Chair. Additional people helping in the committee are not considered voting members. The seven (7) voting members for each standing committees shall be elected semi-annually by all of the members of that committee at the time the election is held.

A. Marketing Committee

1) This committee shall identify and develop ways to promote the LPPA and its agenda to the general public.

2) This committee shall assist the Finance Committee in identifying potential donors and contributors and in developing strategies to build the base of Party supporters.

3) This committee will assist with Marketing content for social media.

C. Newsletter Committee

1. The Committee shall prepare the Libertarian Penn newsletter for periodic publication.

2. The Committee shall determine the distribution, form, and the number of issues.

3. More on this committee (including the reasons for it to operate outside the typical working committee rules) can be found within the communications policy located Section V, item number 9 within this policy manual.

4. This committee shall operate using the Bylaws, Constitution, and the

Philosophy of the party as a conscience for what it writes.

D. Youth Outreach Committee

1) This committee will be responsible for bringing the message of the LPPA to College and High School aged youth who are participating in liberty minded

activities. i.e. "smokedown prohibition" "End The Fed" rallies, etc.

2) Members of this committee will be responsible for visitations to school / university Liberty clubs, i.e. Students for Liberty.

3) This committee will find volunteer work for interested youth in order to bring them in, help them understand what we are about, and encourage growth within the party.

E. Platform Committee

1. This committee shall develop proposals regarding the Platform and/or Statement of Principles for consideration of delegates at the annual LPPA convention.

F. Convention Committee

1. The role of this Committee shall be generally filled by County Committees to prepare the annual LPPA Convention, and may be staffed as necessary.

2. The Convention Committee will be responsible for the details of the convention. (The convention facilitator may or may not be a member of the Convention Committee depending on his/her interest)

a. Details will include, but will not be limited to:

i. Ensuring That changes to the Bylaws are available for the voting body.

ii. Ensuring that notification is sent and the convention is being handled in accordance with the Bylaws

3. Credentialing shall be handled by the Membership Committee and informed by the Treasurer.

4. The Committee should use the CRM to help maintain accurate convention records.

## VIII. COORDINATORS

Coordinators are volunteers of the state party assigned by the Board to help State Leaders manage volunteers and organize assigned tasks in specific areas of need.

1)  It is the responsibility of a Volunteer Coordinator to work with State Leaders to oversee the development of active volunteers. This may include handling volunteer orientation, guidance, and relation care.

2) It is the responsibility of a Event Coordinator to work with state and local affiliate secretaries to notify subscribers of upcoming events. This may include updating the event      calendar, sending reminder messages, and teaching new secretaries how to manage events themselves.

## IX. Contact Relationship (CR) Database Use Policy

We use a Customer relationship management (CRM); The Data may be used as follows:

a) Distribution of the Libertarian Penn Newsletter, and any LPPA project mailing authorized by the Board of Directors.

b) Any recognized county or regional committee may use The Data within their respective electoral districts to pursue candidate activities or other political party activities.

c) An approved candidate shall be an individual who has the nomination or formal endorsement of any county/regional chapter, or if a candidate is in an area where there is no county/regional chapter, the endorsement/nomination will come from the LPPA.

d) Any standing or working committee volunteer may use The Data within their assigned systems role, as required.

e) In addition, anyone pursuing a position in the state party, or seeking the nomination/endorsement of county, regional, or the LPPA, may be approved by a county or regional committee, or, if it is a statewide request, or a request for an area where there is no active county or regional committee, the LPPA Board of Directors will be responsible for approving or denying such requests.

8) Limitations on Use

a) Authorized county or regional committee personnel may use LPPA member data and its own affiliate data, in perpetuity.

b) Endorsed candidates can use CR Data for the duration of their campaign.

c) LPPA Member records will be provided consistent with restrictions specified in the Member Privacy Policy.

d) All working and standing committee volunteers can use The Data for the duration of their assigned role.

Anyone who has access to the CR database will, before receiving access, sign an NDA (Non-Disclosure Agreement)

9) Request procedure

a) Requests for CR data shall be directed to the County or Regional Affiliates if it is specific to a county or regional chapter, and, for those requests made directly to the LPPA, they may be made to the Membership Chair, or any other member of the Executive Committee.

b) Requests shall include the Request/NDA form. If a completed form does not accompany an email request, whomever receives the request will send the form to the requestor, and if this form is not returned, the request will not be considered.

c) The keeper of the CR data is charged with distributing data in accordance with this policy; the Secretary of the LPPA shall keep records of all requests for member data for a period of three years and shall periodically report to the Board of Directors on such requests.

d) All requests for data shall be acted upon as soon as practical. If the requestor has not received a response within 1 calendar week, he/she should contact the chair directly (unless any vice chairs wish to be contacted.

## X. CONVENTION

The annual LPPA convention will be held according to the arrangements of the Convention Committee pursuant to the provisions of the LPPA Constitution, Bylaws, and Convention Rules.

## XI. NOMINATIONS OF CANDIDATES FOR OFFICE

1. The Election Committee may make recommendations to the Board of Directors as to the appropriateness of candidates to run as Libertarian Party nominees.

2. A candidate questionnaire has been prepared, and while it will be voluntary, everyone seeking the Libertarian nomination would do well by answering the questions.

3. At no time shall the Board of Directors nominate a candidate for district or local office prior to one year before an election for that office.

4. Procedure for district or local nominations by the Board of Directors in the absence of County or Regional Committees;

a. Prior to the Board of Directors nominating a district or local candidate in the absence of a county or regional organization, the LPPA Secretary shall, to the extent practical, notify LPPA members in the counties

included in the district or local office of the request for nomination. This will be done prior to the nomination in an effort to receive input from the local

Libertarians.

b. An announcement of the request for nomination shall be placed on the LPPA website. Nominations will be considered at Board Meetings, unless practical circumstances prevent this.

c. The nomination of a candidate other than at a regular meeting of the Board shall be conducted in accordance with the provisions of a special election pursuant to Article V, Section 2 of the bylaws.

## XII. STATEMENT OF PRINCIPLES AND PLATFORM

All changes to the Statement of Principles and Party Platform shall be conducted in accordance with the Bylaws.

## XIII. AMENDMENTS

All amendments will be handled by the Board of Directors, as needed. A Simple Majority may change a given policy.

## XIV. PARLIAMENTARY AUTHORITY

The Parliamentarian shall act formally at the annual LPPA convention; and cannot vote (unless the vote is by ballot) or make motions or debate them---but may provide points of information, as well as provide parliamentary opinions. The Parliamentarian shall act informally at Board of Director meetings and so may vote, make motions, and debate if

a member of the Board of Directors. The Parliamentarian shall be familiar with the latest

edition Robert's Rules of Order Newly Revised.

## XV. Data Maintenance Policy

Nobody shall have any authority to delete approved party records. Once a record is approved by the board, having anything to do with party business, said record shall become property of the party and thus subject to this policy. However, we may enable multiple copies to be maintained.
